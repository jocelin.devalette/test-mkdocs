# Les bases du Markdown

## Vocabulaire et objectifs

## Construire une note

### Architecture

Avec croisillons.

### Paragraphes

ligne1   
ligne2

Paragraphe 1...

---

Paragraphe 2...

### Listes

Avec `-`, ou `1.`

Markdown est **très** utilisé dans de _nombreuses_ situations

- Jupyter
- MkDocs
- Discourse
- GitLab
- Reddit
- Qt
- Stack Overflow
- Stack Exchange
- ...

Imbriquées...

On va faire une liste

1. un
1. deux
    - machin
    - chose
        31. Pour commencer à 31
        1984. Ce ne sera pas 1984 !
1. trois



### Liens

url et image

### Emphase

`_` et `*`

### Citations

`>`

### Bloc de code

```python
for i in range(10):
    print("Salut à tous !")
```

```c
#include<stdio.h>
int main(){
    int i;
    for(i = 0; i < 10; i++){
        printf("Salut à tous !\n");
    }
    return 0;
}
```

### Code en ligne

Avec `` ` ``

### Ligne horizontale

Avec `---`

Une liste de tâches

- [ ] à faire
- [x] fini
- [ ] presque
- [x] fait depuis longtemps

test Latex 

$f(x) = x^2_i$

!!! example

    === "Unordered List"

        ``` markdown
        * Sed sagittis eleifend rutrum
        * Donec vitae suscipit est
        * Nulla tempor lobortis orci
        ```

    === "Ordered List"

        ``` markdown
        1. Sed sagittis eleifend rutrum
        2. Donec vitae suscipit est
        3. Nulla tempor lobortis orci
        ```
